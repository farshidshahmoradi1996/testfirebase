
importScripts('https://www.gstatic.com/firebasejs/8.1.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.1.2/firebase-messaging.js');
firebase.initializeApp({
     apiKey: "AIzaSyAo-9q4Gnr5t7n9NRYncTAnzw4leSfcmG4",
  authDomain: "drsaina-draft.firebaseapp.com",
  projectId: "drsaina-draft",
  storageBucket: "drsaina-draft.appspot.com",
  messagingSenderId: "107140139185",
  appId: "1:107140139185:web:6f35900cfe955d2783fa90",
  measurementId: "G-SP2R4N4904"
});

const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function(payload) {
     const promiseChain = clients
          .matchAll({
               type: "window",
               includeUncontrolled: true,
          })
          .then((windowClients) => {
               for (let i = 0; i < windowClients.length; i++) {
                    const windowClient = windowClients[i];
                    windowClient.postMessage(payload);
               }
          })
          .then(() => {
               return registration.showNotification("my notification title");
          });
     return promiseChain;
});
self.addEventListener("notificationclick", function(event) {
     console.log(event);
});