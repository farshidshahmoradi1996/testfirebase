import React, { useEffect } from "react";
import "./App.css";
import  firebase from "firebase/app";

function App() {
  useEffect(() => {
    const messaging = firebase.messaging();
    messaging.getToken().then(res=>console.log(res)).catch(e=>console.log(e));
  }, []);
  return (
    <div className="App">
        Hello Babe !
    </div>
  );
}

export default App;
