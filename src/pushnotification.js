import firebase from 'firebase/app';
import 'firebase/messaging';
const firebaseConfig = {
  apiKey: "AIzaSyAo-9q4Gnr5t7n9NRYncTAnzw4leSfcmG4",
  authDomain: "drsaina-draft.firebaseapp.com",
  projectId: "drsaina-draft",
  storageBucket: "drsaina-draft.appspot.com",
  messagingSenderId: "107140139185",
  appId: "1:107140139185:web:6f35900cfe955d2783fa90",
  measurementId: "G-SP2R4N4904"
  };
export const initializeFirebase = () => {

  firebase.initializeApp(firebaseConfig);
};
export const askForPermissioToReceiveNotifications = async () => {
  try {
    const messaging = firebase.messaging();
    await messaging.requestPermission();
  } catch (error) {
    console.error(error);
  }
};
